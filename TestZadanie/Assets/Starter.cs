﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Starter : MonoBehaviour
{
    [SerializeField] GameCore gameCore;
    [SerializeField] GameUI gameUI;

    private void Awake()
    {
        gameCore = Instantiate(gameCore);
        gameUI = Instantiate(gameUI);
    }
}
