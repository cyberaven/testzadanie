﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCore : MonoBehaviour
{
    [SerializeField] StateMashine stateMashine;
    [SerializeField] private GameField gameFieldPrefab;
    private GameField currentGameField;

    private void Start()
    {
        stateMashine = Instantiate(stateMashine, transform);

        GameField.RegenerateFieldEvent += RegenerateField;

        CreateGameField();
    }
    private void OnDestroy()
    {
        GameField.RegenerateFieldEvent -= RegenerateField;
    }
    private void RegenerateField()
    {        
        Destroy(currentGameField.gameObject);
        CreateGameField();
    }
    private void CreateGameField()
    {
        currentGameField = Instantiate(gameFieldPrefab);
    }
}
