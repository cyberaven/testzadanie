﻿using UnityEngine;
using System.Collections;

public enum GameState
{
    Start,
    CheckField,
    WaitPlayer,
    CheckComb,
    DelComb,
    MoveToken,
    NewToken
}


public class StateMashine : MonoBehaviour
{
    public static StateMashine INSTANCE;

    [SerializeField] GameState currentGameState;
    public GameState CurrentGameState
    {
        get
        {
            return currentGameState;
        }
        set
        {
            currentGameState = value;
            if(GameStateChangeEvent != null)
            {
                GameStateChangeEvent();
            }
        }
    }

    public delegate void GameStateChangeDel();
    public static event GameStateChangeDel GameStateChangeEvent;

    private void Awake()
    {
        INSTANCE = this;
    }
}
