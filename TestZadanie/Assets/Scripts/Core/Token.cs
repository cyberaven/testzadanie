﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public enum TokenColor
{
    Red,
    Green,
    Blue,
    Yellow
}

public class Token : MonoBehaviour
{
    [SerializeField] private SpriteRenderer view;

    [SerializeField] private TokenColor color;
    public TokenColor Color
    {
        get
        {
            return color;
        }
        set
        {
            color = value;
            ChangeColor();
        }
    }    

    [SerializeField] private bool toDel = false;
    public bool ToDel { get => toDel; set => toDel = value; }
    private bool destroy = false;

    [SerializeField] GameObject selectDot;
    [SerializeField] private float moveSpeed = 10;

    public delegate void TokenSelectDel(Token token);
    public static event TokenSelectDel TokenSelectEvent;

    private void Awake()
    {
        RandomColor();
    }
    private void Update()
    {
        MoveToCentrGFC();       
    }
    private void OnMouseDown()
    {
        if (StateMashine.INSTANCE.CurrentGameState == GameState.WaitPlayer)
        {
            Select();
        }
    }
    private void Select()
    {
        selectDot.SetActive(true);

        if (TokenSelectEvent != null)
        {
            TokenSelectEvent(this);
        }        
    }
    public void Deselect()
    {
        StartCoroutine(Wait());               
    }
    public void Destroy()
    {
        Destroy(gameObject);        
    }
    private void MoveToCentrGFC()
    {
        if(transform.localPosition != Vector3.zero)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, Time.deltaTime * moveSpeed);
        }       
    }
    private void RandomColor()
    {
        Color = (TokenColor)Enum.GetValues(typeof(TokenColor)).GetValue(UnityEngine.Random.Range(0, Enum.GetValues(typeof(TokenColor)).Length));        
    }
    private void ChangeColor()
    {
        if(color == TokenColor.Blue)
        {
            view.color = UnityEngine.Color.blue;
        }
        if (color == TokenColor.Green)
        {
            view.color = UnityEngine.Color.green;
        }
        if (color == TokenColor.Red)
        {
            view.color = UnityEngine.Color.red;
        }
        if (color == TokenColor.Yellow)
        {
            view.color = UnityEngine.Color.yellow;
        }
    }   
    IEnumerator Wait()
    {        
        yield return new WaitForSeconds(0.3f);
        selectDot.SetActive(false);        
    }    
}
