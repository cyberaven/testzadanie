﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameField : MonoBehaviour
{
    private int sizeX = 6;
    private int sizeY = 6;
    private int offGFCOnField = 3;

    [SerializeField] private GameFieldCell gameFieldCellPrefab;
    private List<GameFieldCell> activeGameFieldCells = new List<GameFieldCell>();

    [SerializeField] private Token tokenPrefab;

    [SerializeField] private Token selectToken1;
    [SerializeField] private Token selectToken2;

    List<MoveHistoryData> moveHistory = new List<MoveHistoryData>();

    public delegate void RepackFieldDel();
    public static event RepackFieldDel RepackFieldEvent;

    public delegate void RegenerateFieldDel();
    public static event RegenerateFieldDel RegenerateFieldEvent;

    public delegate void ChangeScoreDel(int score);
    public static event ChangeScoreDel ChangeScoreEvent;

    private void Start()
    {
        Token.TokenSelectEvent += TokenSelect;
        GameFieldCell.GFCTakeToken += GFCTakeToken;
        MainScreen.RepackBtnClkEvent += RePackField;

        StateMashine.INSTANCE.CurrentGameState = GameState.Start;
        StartCoroutine(Main());
    }
    private void OnDestroy()
    {
        Token.TokenSelectEvent -= TokenSelect;
        GameFieldCell.GFCTakeToken -= GFCTakeToken;
        MainScreen.RepackBtnClkEvent -= RePackField;
    }
    IEnumerator Main()
    {
        while (true)
        {            
            yield return null;
            if (StateMashine.INSTANCE.CurrentGameState == GameState.Start)
            {
                SetupStartGamefield();
                Debug.Log("Start");
            }
            else if (StateMashine.INSTANCE.CurrentGameState == GameState.CheckField)
            {
                CheckGameField();
                Debug.Log("CheckField");
            }
            else if (StateMashine.INSTANCE.CurrentGameState == GameState.WaitPlayer)
            {              
              
            }
            else if (StateMashine.INSTANCE.CurrentGameState == GameState.CheckComb)
            {
                FindCombination();
                Debug.Log("CheckComb");
            }
            else if (StateMashine.INSTANCE.CurrentGameState == GameState.DelComb)
            {
                DeleteTokenInCombination();               
                Debug.Log("DeleteToken");
            }
            else if (StateMashine.INSTANCE.CurrentGameState == GameState.MoveToken)
            {
                AllTokenMoveUP();
                Debug.Log("MoveToken");
            }
            else if (StateMashine.INSTANCE.CurrentGameState == GameState.NewToken)
            {
                FillRandomGameField();
                Debug.Log("NewToken");
            }
        }
    }

    #region SetupStartGamefield
    private void SetupStartGamefield()
    {
        CreateField(sizeX, sizeY);
        SelectGFCForOff(offGFCOnField);
        FillRandomGameField();
        CheckGameField();
    }
    private void CreateField(int sizeX, int sizeY)
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                GameFieldCell gFC = Instantiate(gameFieldCellPrefab, transform);
                gFC.transform.localPosition = new Vector3(x, y, 0);
                activeGameFieldCells.Add(gFC);
            }
        }
    }
    private void SelectGFCForOff(int count)
    {
        List<GameFieldCell> gFCL = new List<GameFieldCell>();

        int a = 100;//защита от бесконечного перебора
        while (a > 0 && gFCL.Count < count)
        {
            GameFieldCell gFC = SelectRandomGFC();

            if (!gFCL.Contains(gFC))
            {
                gFCL.Add(gFC);
            }
            a--;
        }

        foreach (GameFieldCell gfc in gFCL)
        {
            gfc.ActiveStatus = ActiveStatus.Off;
        }
    }
    private GameFieldCell SelectRandomGFC()
    {
        return activeGameFieldCells[Random.Range(0, activeGameFieldCells.Count)];
    }
    private void FillRandomGameField()
    {       
        for (int x = 0; x < sizeX; x++)
        {
            List<GameFieldCell> gfcL = GetGameFieldsColum(x);
            int i = 0;
            foreach (GameFieldCell gameFieldCell in gfcL)
            {
                if (gameFieldCell.ActiveStatus == ActiveStatus.Off || gameFieldCell.Token != null)
                {
                    i++;
                }
            }
            for (int y = 0; y < sizeY - i; y++)
            {
                foreach (GameFieldCell gfc1 in gfcL)
                {
                    if (gfc1.ActiveStatus == ActiveStatus.On && gfc1.Token == null)
                    {
                        Token t = Instantiate(tokenPrefab);
                        t.transform.position = new Vector3(x, -1, 0);
                        gfc1.Token = t;
                        break;
                    }
                }
            }
        }
        if(StateMashine.INSTANCE.CurrentGameState != GameState.Start)
        {
            StateMashine.INSTANCE.CurrentGameState = GameState.CheckComb;
        }
    }
    private void CheckGameField()
    {
        bool combFind = FindCombination();
        bool futureCombFind = FindFutureCombination();

        if (combFind || !futureCombFind)
        {
            if (StateMashine.INSTANCE.CurrentGameState == GameState.Start)
            {
                ReGenerateField();
            }
            else
            {
                RePackField();
            }
        }
        else
        {
            UndoMove();
            StateMashine.INSTANCE.CurrentGameState = GameState.WaitPlayer;
        }
    }
    private void UndoMove()
    {
        if (moveHistory.Count > 0)
        {
            StartCoroutine(UMove());            
        }
    }
    IEnumerator UMove()
    {
        yield return new WaitForSeconds(0.5f);
        MoveHistoryData mhd1 = moveHistory[moveHistory.Count - 1];
        MoveHistoryData mhd2 = moveHistory[moveHistory.Count - 2];

        GameFieldCell gfc1 = mhd1.gameFieldCell;
        Token t1 = mhd1.token;
        GameFieldCell gfc2 = mhd2.gameFieldCell;
        Token t2 = mhd2.token;

        gfc1.Token = t1;
        gfc2.Token = t2;
    }
    #endregion

    #region FindCombination
    private bool FindCombination()
    {
        bool result = false;       

        CheckAllRow();
        CheckAllColum();

        foreach (GameFieldCell gameFieldCell in activeGameFieldCells)
        {
            if (gameFieldCell.Token != null && gameFieldCell.Token.ToDel == true)
            {
                result = true;
            }
        }

        if (StateMashine.INSTANCE.CurrentGameState == GameState.CheckComb)
        {
            if (result == true)
            {
                moveHistory = new List<MoveHistoryData>();
                StateMashine.INSTANCE.CurrentGameState = GameState.DelComb;               
            }
            else
            {
                if(IsFullGameField())
                {
                    StateMashine.INSTANCE.CurrentGameState = GameState.CheckField;
                }
                else
                {
                    StateMashine.INSTANCE.CurrentGameState = GameState.NewToken;
                }                         
            }                
        }           
        return result;
    }
    private void CheckAllRow()
    {
        for (int y = 0; y < sizeY; y++)
        {
            CheckRow(y);
        }
    }
    private void CheckRow(int y)
    {
        List<GameFieldCell> gfcRow = GetGameFieldsRow(y);

        for (int i = 0; i < gfcRow.Count; i++)
        {
            List<GameFieldCell> resultList = new List<GameFieldCell>();
            resultList.Add(gfcRow[i]);

            for (int j = 0; j < gfcRow.Count; j++)
            {
                if (CompareTokenColor(resultList[resultList.Count - 1], gfcRow[j]))
                {
                    if (IsXNeighbourhood(gfcRow[j], resultList[resultList.Count - 1]))
                    {
                        resultList.Add(gfcRow[j]);
                    }
                }
            }

            if (resultList.Count > 2)
            {
                Debug.Log("Combination!");
                MarkTokenToDel(resultList);
            }
        }
    }
    private void CheckAllColum()
    {
        for (int x = 0; x < sizeX; x++)
        {
            CheckColum(x);
        }
    }
    private void CheckColum(int x)
    {
        List<GameFieldCell> gfcColum = GetGameFieldsColum(x);

        for (int i = 0; i < gfcColum.Count; i++)
        {
            List<GameFieldCell> resultList = new List<GameFieldCell>();
            resultList.Add(gfcColum[i]);

            for (int j = 0; j < gfcColum.Count; j++)
            {
                if (CompareTokenColor(resultList[resultList.Count - 1], gfcColum[j]))
                {
                    if (IsYNeighbourhood(gfcColum[j], resultList[resultList.Count - 1]))
                    {
                        resultList.Add(gfcColum[j]);
                    }
                }
            }

            if (resultList.Count > 2)
            {
                Debug.Log("Combination!");
                MarkTokenToDel(resultList);
            }
        }
    }
    #endregion

    #region FindFutureCombination
    private bool FindFutureCombination()
    {
        bool result = false;

        foreach (GameFieldCell gameFieldCell in activeGameFieldCells)
        {
            if (CheckPaternThroughOne(gameFieldCell))
            {
                return result = true;
            }
            if (CheckPaternTwoNeighbourhood(gameFieldCell))
            {
                return result = true;
            }
        }
        return result;
    }
    private bool CheckPaternThroughOne(GameFieldCell gfc)
    {
        bool result = false;

        if (CheckPaternThroughOneX(gfc))
        {
            return result = true;
        }
        if (CheckPaternThroughOneY(gfc))
        {
            return result = true;
        }
        return result;
    }
    private bool CheckPaternThroughOneX(GameFieldCell gfc)
    {
        bool result = false;

        Vector3 gfcTLP = gfc.transform.localPosition;
        List<GameFieldCell> gfcLX = new List<GameFieldCell>();
        gfcLX = GetGFCThroughOneXSimilarColor(gfc);

        if (gfcLX.Count > 0)
        {
            foreach (GameFieldCell gfcI in gfcLX)
            {
                if (gfcTLP.x > gfcI.transform.localPosition.x)
                {
                    if (CompareTokenColor(GetGameFieldCell(gfcTLP.x - 1, gfcTLP.y + 1), gfc) || CompareTokenColor(GetGameFieldCell(gfcTLP.x - 1, gfcTLP.y - 1), gfc))
                    {
                        return result = true;
                    }
                }
            }
        }
        return result;
    }
    private bool CheckPaternThroughOneY(GameFieldCell gfc)
    {
        bool result = false;

        Vector3 gfcTLP = gfc.transform.localPosition;
        List<GameFieldCell> gfcLY = new List<GameFieldCell>();
        gfcLY = GetGFCThroughOneYSimilarColor(gfc);

        if (gfcLY.Count > 0)
        {
            foreach (GameFieldCell gfcI in gfcLY)
            {
                if (gfcTLP.y > gfcI.transform.localPosition.y)
                {
                    if (CompareTokenColor(GetGameFieldCell(gfcTLP.x - 1, gfcTLP.y - 1), gfc) || CompareTokenColor(GetGameFieldCell(gfcTLP.x + 1, gfcTLP.y - 1), gfc))
                    {
                        return result = true;
                    }
                }
            }
        }
        return result;
    }
    private bool CheckPaternTwoNeighbourhood(GameFieldCell gfc)
    {
        bool result = false;

        if (CheckPaternTwoNeighbourhoodX(gfc))
        {
            return result = true;
        }
        if (CheckPaternTwoNeighbourhoodY(gfc))
        {
            return result = true;
        }
        return result;
    }
    private bool CheckPaternTwoNeighbourhoodX(GameFieldCell gfc)
    {
        bool result = false;

        Vector3 gfcTLP = gfc.transform.localPosition;

        if (CompareTokenColor(GetGameFieldCell(gfcTLP.x - 1, gfcTLP.y), gfc))
        {
            if (CompareTokenColor(GetGameFieldCell(gfcTLP.x - 3, gfcTLP.y), gfc))
            {
                return result = true;
            }
        }
        if (CompareTokenColor(GetGameFieldCell(gfcTLP.x + 1, gfcTLP.y), gfc))
        {
            if (CompareTokenColor(GetGameFieldCell(gfcTLP.x + 3, gfcTLP.y), gfc))
            {
                return result = true;
            }
        }
        return result;
    }
    private bool CheckPaternTwoNeighbourhoodY(GameFieldCell gfc)
    {
        bool result = false;
        Vector3 gfcTLP = gfc.transform.localPosition;

        if (CompareTokenColor(GetGameFieldCell(gfcTLP.x, gfcTLP.y + 1), gfc))
        {
            if (CompareTokenColor(GetGameFieldCell(gfcTLP.x, gfcTLP.y + 3), gfc))
            {
                return result = true;
            }
        }
        if (CompareTokenColor(GetGameFieldCell(gfcTLP.x, gfcTLP.y - 1), gfc))
        {
            if (CompareTokenColor(GetGameFieldCell(gfcTLP.x, gfcTLP.y - 3), gfc))
            {
                return result = true;
            }
        }
        return result;
    }
    private List<GameFieldCell> GetGFCThroughOneXSimilarColor(GameFieldCell gfc)
    {
        List<GameFieldCell> result = new List<GameFieldCell>();

        GameFieldCell gfc1 = GetGameFieldCell(gfc.transform.localPosition.x + 2, gfc.transform.localPosition.y);
        if (gfc1 != null)
        {
            if (CompareTokenColor(gfc, gfc1))
            {
                result.Add(gfc1);
            }
        }
        GameFieldCell gfc2 = GetGameFieldCell(gfc.transform.localPosition.x - 2, gfc.transform.localPosition.y);
        if (gfc2 != null)
        {
            if (CompareTokenColor(gfc, gfc2))
            {
                result.Add(gfc2);
            }
        }
        return result;
    }
    private List<GameFieldCell> GetGFCThroughOneYSimilarColor(GameFieldCell gfc)
    {
        List<GameFieldCell> result = new List<GameFieldCell>();
        GameFieldCell gfc1 = GetGameFieldCell(gfc.transform.localPosition.x, gfc.transform.localPosition.y + 2);
        if (gfc1 != null)
        {
            if (CompareTokenColor(gfc, gfc1))
            {
                result.Add(gfc1);
            }
        }
        GameFieldCell gfc2 = GetGameFieldCell(gfc.transform.localPosition.x, gfc.transform.localPosition.y - 2);
        if (gfc2 != null)
        {
            if (CompareTokenColor(gfc, gfc2))
            {
                result.Add(gfc2);
            }
        }
        return result;
    }
    #endregion

    #region Work With Token    
    private void AllTokenMoveUP()
    {
        StartCoroutine(TokenMoveUP());        
    }
    IEnumerator TokenMoveUP()
    {        
        for (int i = activeGameFieldCells.Count - 1; i >= 0; i--)
        {
            if (activeGameFieldCells[i].Token != null)
            {
                activeGameFieldCells[i].Token = activeGameFieldCells[i].Token;
            }
        }
        yield return new WaitForSeconds(0.5f);
        StateMashine.INSTANCE.CurrentGameState = GameState.CheckComb;
    }
    private void TokenSelect(Token token)
    {
        if (StateMashine.INSTANCE.CurrentGameState == GameState.WaitPlayer)
        {
            if (selectToken1 == null)
            {
                selectToken1 = token;
            }
            else
            {
                if (token != selectToken1)
                {
                    selectToken2 = token;
                }
            }
            if (selectToken1 != null && selectToken2 != null)
            {
                CheckToChangeTokenPlaces(selectToken1, selectToken2);
            }
        }
    }
    private void CheckToChangeTokenPlaces(Token t1, Token t2)
    {
        bool change = false;

        GameFieldCell gfc1 = GetGameFieldCell(t1);
        GameFieldCell gfc2 = GetGameFieldCell(t2);

        if (CompareXLocalPosition(gfc1, gfc2) && IsYNeighbourhood(gfc2, gfc1))
        {
            change = true;
        }
        if (CompareXLocalPosition(gfc1, gfc2) && IsYNeighbourhood(gfc1, gfc2))
        {
            change = true;
        }
        if (CompareYLocalPosition(gfc1, gfc2) && IsXNeighbourhood(gfc1, gfc2))
        {
            change = true;
        }
        if (CompareYLocalPosition(gfc1, gfc2) && IsXNeighbourhood(gfc2, gfc1))
        {
            change = true;
        }

        if (change == true)
        {
            ChangeTokenPlaces(t1, t2);
        }
        else
        {
            DeselectToken(t1);
        }
    }
    private void ChangeTokenPlaces(Token t1, Token t2)
    {
        GameFieldCell gfc1 = GetGameFieldCell(t1);
        GameFieldCell gfc2 = GetGameFieldCell(t2);
        gfc1.Token = t2;
        gfc2.Token = t1;

        moveHistory = new List<MoveHistoryData>();

        moveHistory.Add(new MoveHistoryData(gfc1, t1));
        moveHistory.Add(new MoveHistoryData(gfc2, t2));

        DeselectToken(t1, t2);        

        StateMashine.INSTANCE.CurrentGameState = GameState.CheckComb;
    }
    private void DeleteTokenInCombination()
    {
        StartCoroutine(DeleteToken());        
    }
    IEnumerator DeleteToken()
    {
        yield return new WaitForSeconds(0.3f);
        int delCount = 0;
        foreach (GameFieldCell gfc in activeGameFieldCells)
        {
            if (gfc.Token != null && gfc.Token.ToDel == true)
            {
                gfc.Token.Destroy();
                delCount++;
            }
        }
        if (delCount == 3)
        {
            if (ChangeScoreEvent != null)
            {
                ChangeScoreEvent(10);
            }
        }
        else if (delCount == 4)
        {
            if (ChangeScoreEvent != null)
            {
                ChangeScoreEvent(15);
            }
        }
        else if (delCount == 5)
        {
            if (ChangeScoreEvent != null)
            {
                ChangeScoreEvent(20);
            }
        }
        StateMashine.INSTANCE.CurrentGameState = GameState.MoveToken;
    }
    private void MarkTokenToDel(List<GameFieldCell> gfcL)
    {
        foreach (GameFieldCell gameFieldCell in gfcL)
        {
            gameFieldCell.Token.ToDel = true;
        }
    }
    private void GFCTakeToken(GameFieldCell gfc)
    {
        if (gfc.transform.localPosition.y != sizeY)
        {                     
            Vector3 gfcTLP = gfc.transform.localPosition;
            List<GameFieldCell> gfcYC = GetGameFieldsColum(gfcTLP.x);

            for (int i = 0; i < gfcYC.Count; i++)
            {
                if (gfcYC[i] == gfc)
                {
                    for (int j = 1; j < gfcYC.Count - i; j++)
                    {
                        if (gfcYC[i + j] != null)
                        {
                            if (gfcYC[i + j].ActiveStatus == ActiveStatus.On)
                            {
                                if (gfcYC[i + j].Token == null)
                                {
                                    gfcYC[i + j].Token = gfc.Token;
                                    gfc.Token = null;
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }   
    private void DeselectToken(Token t)
    {
        t.Deselect();
        selectToken1 = selectToken2;
        selectToken2 = null;
    }
    private void DeselectToken(Token t1, Token t2)
    {
        t1.Deselect();
        t2.Deselect();
        selectToken1 = null;
        selectToken2 = null;        
    }
    #endregion

    #region Work With GameFieldCell

    #region GetGameFieldCell
    private GameFieldCell GetGameFieldCell(float x, float y)
    {
        GameFieldCell result = null;

        foreach (GameFieldCell gfc in activeGameFieldCells)
        {
            Vector3 gfcTLP = gfc.transform.localPosition;

            if ((int)gfcTLP.x == (int)x && (int)gfcTLP.y == (int)y)
            {
                result = gfc;
            }
        }
        return result;
    }
    private GameFieldCell GetGameFieldCell(Token token)
    {
        GameFieldCell result = null;

        foreach (GameFieldCell gfc in activeGameFieldCells)
        {
            if (gfc.Token == token)
            {
                return gfc;
            }
        }
        return result;
    }
    #endregion

    #region Get GFC Row/Colum
    private List<GameFieldCell> GetGameFieldsRow(float y)
    {
        List<GameFieldCell> result = new List<GameFieldCell>();

        foreach (GameFieldCell gfc in activeGameFieldCells)
        {
            Vector3 gfcTLP = gfc.transform.localPosition;

            if ((int)gfcTLP.y == (int)y)
            {
                result.Add(gfc);
            }
        }
        return result;
    }
    private List<GameFieldCell> GetGameFieldsColum(float x)
    {
        List<GameFieldCell> result = new List<GameFieldCell>();

        foreach (GameFieldCell gfc in activeGameFieldCells)
        {
            Vector3 gfcTLP = gfc.transform.localPosition;

            if ((int)gfcTLP.x == (int)x)
            {
                result.Add(gfc);
            }
        }
        return result;
    }
    #endregion

    #region Compare GFC
    private bool CompareXLocalPosition(GameFieldCell gfc1, GameFieldCell gfc2)
    {
        bool result = false;
        if ((int)gfc1.transform.localPosition.x == (int)gfc2.transform.localPosition.x)
        {
            return result = true;
        }
        return result;
    }
    private bool CompareYLocalPosition(GameFieldCell gfc1, GameFieldCell gfc2)
    {
        bool result = false;
        if ((int)gfc1.transform.localPosition.y == (int)gfc2.transform.localPosition.y)
        {
            return result = true;
        }
        return result;
    }
    private bool CompareTokenColor(GameFieldCell gfc1, GameFieldCell gfc2)
    {
        bool result = false;
        if (gfc1 != null && gfc2 != null)
        {
            if (gfc1.Token != null && gfc2.Token != null && gfc1.Token.Color == gfc2.Token.Color)
            {
                result = true;
            }
        }
        return result;
    }
    private bool IsXNeighbourhood(GameFieldCell gfc1, GameFieldCell gfc2)
    {
        bool result = false;
        if ((int)gfc1.transform.localPosition.x == (int)gfc2.transform.localPosition.x + 1)
        {
            result = true;
        }
        return result;
    }
    private bool IsYNeighbourhood(GameFieldCell gfc1, GameFieldCell gfc2)
    {
        bool result = false;
        if ((int)gfc1.transform.localPosition.y == (int)gfc2.transform.localPosition.y + 1)
        {
            result = true;
        }
        return result;
    }
    #endregion
        
    private bool IsFullGameField()
    {
        bool result = true;
        foreach (GameFieldCell gameFieldCell in activeGameFieldCells)
        {
            if(gameFieldCell.ActiveStatus == ActiveStatus.On)
            {
                if(gameFieldCell.Token == null)
                {
                    return result = false;
                }
            }
        }
        return result;
    }
    private void RePackField()
    {
        List<Token> tL = new List<Token>();
        List<GameFieldCell> gfcL = new List<GameFieldCell>();

        System.Random random = new System.Random();

        foreach (GameFieldCell gameFieldCell in activeGameFieldCells)
        {
            if (gameFieldCell.ActiveStatus == ActiveStatus.On)
            {
                gfcL.Add(gameFieldCell);
                tL.Add(gameFieldCell.Token);
            }
        }
        for (int i = tL.Count - 1; i >= 1; i--)
        {
            int j = random.Next(i + 1);
            Token temp = tL[j];
            tL[j] = tL[i];
            tL[i] = temp;
        }
        for (int a = 0; a < gfcL.Count; a++)
        {
            gfcL[a].Token = tL[a];
        }

        StateMashine.INSTANCE.CurrentGameState = GameState.CheckComb;

        if (RepackFieldEvent != null)
        {
            RepackFieldEvent();
        }
    }
    #endregion

    #region Events
    private void ReGenerateField()
    {
        Debug.Log("ReGenerateField");
        if (RegenerateFieldEvent != null)
        {
            RegenerateFieldEvent();
        }
    }    

    #endregion
}



