﻿using UnityEngine;
using System.Collections;

public class MoveHistoryData
{
    public Token token;
    public GameFieldCell gameFieldCell;

    public MoveHistoryData(GameFieldCell gfc, Token t)
    {
        gameFieldCell = gfc;
        token = t;
    }
}
