﻿using UnityEngine;
using System.Collections;

public enum ActiveStatus
{
    On,
    Off
}
public class GameFieldCell : MonoBehaviour
{
    [SerializeField] private ActiveStatus activeStatus = ActiveStatus.On;
    public ActiveStatus ActiveStatus
    {
        get
        {
            return activeStatus;
        }
        set
        {
            activeStatus = value;
            ChangeBG();
        }
    }    

    [SerializeField] SpriteRenderer view;

    private Token token;
    public Token Token
    {
        get
        {
            return token;
        }
        set
        {
            token = value;
            if(token != null)
            {
                token.transform.SetParent(transform);
                if (GFCTakeToken != null)
                {
                    GFCTakeToken(this);
                }
            }
        }
    }

    public delegate void GFCTakeTokenDel(GameFieldCell gfc);
    public static event GFCTakeTokenDel GFCTakeToken;

    private void ChangeBG()
    {
        if(activeStatus == ActiveStatus.On)
        {
            view.color = Color.white;
        }
        if(activeStatus == ActiveStatus.Off)
        {
            view.color = Color.grey;
        }
    }
}
