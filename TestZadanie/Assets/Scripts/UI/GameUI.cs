﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    [SerializeField] MainScreen mainScreen;
    private void Awake()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;

        mainScreen = Instantiate(mainScreen, transform);
    }   
}
