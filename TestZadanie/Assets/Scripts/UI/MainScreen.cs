﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainScreen : MonoBehaviour
{
    [SerializeField] Text msgText;
    [SerializeField] Text scoreText;
    [SerializeField] Button repackBtn;
    int score = 0;

    public delegate void RepackBtnClkDel();
    public static event RepackBtnClkDel RepackBtnClkEvent;

    private void Start()
    {
        StateMashine.GameStateChangeEvent += GameStateChange;
        GameField.RepackFieldEvent += RepackField;
        GameField.RegenerateFieldEvent += RegenField;
        GameField.ChangeScoreEvent += ChangeScore;
        repackBtn.onClick.AddListener(RepackBtnClk);
    }
    private void OnDestroy()
    {
        StateMashine.GameStateChangeEvent += GameStateChange;
        GameField.RepackFieldEvent -= RepackField;
        GameField.RegenerateFieldEvent -= RegenField;
        GameField.ChangeScoreEvent += ChangeScore;
    }
    private void RepackField()
    {
        msgText.text = "Find Combination. Repack Field";
    }
    private void RegenField()
    {
        score = 0;
        msgText.text = "Find Combination. Regen Field";
    }
    private void GameStateChange()
    {
        msgText.text = "Game State: " + StateMashine.INSTANCE.CurrentGameState.ToString();
    }

    private void ChangeScore(int newScore)
    {
        score += newScore;

        scoreText.text = "Score: " + score;
    }
    private void RepackBtnClk()
    {
        if(RepackBtnClkEvent != null)
        {
            RepackBtnClkEvent();
        }
    }

}
